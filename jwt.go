package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"github.com/apache/dubbo-go-pixiu/pkg/common/constant"
	"github.com/apache/dubbo-go-pixiu/pkg/common/extension/filter"
	"github.com/apache/dubbo-go-pixiu/pkg/context/http"
	"github.com/dgrijalva/jwt-go"
	"time"
)

const (
	sk = "justForTest"
	expired_time = 24 * time.Hour
	Kind = constant.HTTPJWTFilter
)

var (
	tokenSecretWords = []byte("*YREQ^EQOP#UYUTEY(Q&")
)


// store ak in jwt
type jwtClaim struct {
	Ak string `json:"ak"`
	jwt.StandardClaims
}

// Plugin and JwtFilter type
type (
	Plugin struct {
	}
	JwtFilter struct {
		cfg *Config
	}
	Config struct {
	}
)

func init() {
	filter.RegisterHttpFilter(&Plugin{})
}

func (p *Plugin) Kind() string{
	return Kind
}

func (p *Plugin)CreateFilter() (filter.HttpFilter, error){
	return &JwtFilter{}, nil
}

func (f *JwtFilter) PrepareFilterChain(ctx *http.HttpContext) error {
	ctx.AppendFilterFunc(f.Handle)
	return nil
}

func (f *JwtFilter) Config() interface{}{
	return f.cfg
}

func (f *JwtFilter) Apply() error{
	return nil
}

func (f *JwtFilter) Handle(hc *http.HttpContext){
	api := hc.GetAPI()
	headers := api.Headers
	ak := headers["access_key"]
	signature := headers["signature"]
	startTime := stringToTime(headers["start_time"])

	// if there is token in headers
	if token, ok:= headers["token"]; ok{
		if claims, err := ParseToken(token); err != nil{
			hc.Abort()
			return
		}else if err := TokenIsValid(ak, claims); err != nil { // check if the token is valid
			hc.Abort()
			return
		}else {
			goto Issue
		}
	}

	// if there is no token in headers
	// if ak and sk verification didn't pass
	if _, err := Verify(ak, signature, startTime); err != nil{
		hc.Abort()
		return
	}

	// issue token
	Issue:
		if jwt, err := CreateToken(ak, signature, startTime); err != nil{
			hc.Abort()
			return
		}else {
			hc.Write([]byte(jwt))
			hc.Next()
			return
		}
}

// stringToTime switches string type to time
// note that: time lay out is "2021-08-12 17:44:00"
func stringToTime(timeStr string) time.Time{
	local, _ := time.LoadLocation("Local")
	timeLayOut := "2021-08-12 17:44:00"
	res, _ := time.ParseInLocation(timeLayOut, timeStr, local)
	return res
}

// signature generation by ak and sk
func Sign(ak string) string{
	secret := []byte(sk)
	h := hmac.New(sha256.New, secret)
	h.Write([]byte(ak))
	sha := hex.EncodeToString(h.Sum(nil))
	return base64.StdEncoding.EncodeToString([]byte(sha))
}

// verify that the signature is valid
func Verify(ak string, signature string, startTime time.Time) (bool, error){
	rightSignature := Sign(ak)
	// expired time: 24 hours later
	if time.Since(startTime) > expired_time {
		return false, errors.New("Time out!")
	} else if rightSignature != signature{
		return false, errors.New("The signature is wrong!")
	}else{
		return true, nil
	}
}

// create and issue token if it passed the verification
func CreateToken(ak string, signature string, startTime time.Time) (string, error){
	if ok,err := Verify(ak, signature, startTime); ok{
		claim := jwtClaim{
			ak,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(expired_time).Unix(),
				Issuer: "pixiu",
			},
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
		return token.SignedString(tokenSecretWords)
	}else{
		return "", err
	}
}

// parse token to claims
func ParseToken(tokenString string) (*jwtClaim, error){
	token, err := jwt.ParseWithClaims(tokenString, &jwtClaim{}, func(token *jwt.Token) (interface{}, error) {
		return tokenSecretWords, nil
	})
	if err != nil{
		return nil, err
	}
	if claims, ok := token.Claims.(*jwtClaim); ok && token.Valid{
		return claims, nil
	}
	return nil, errors.New("Invalid token!")
}

func TokenIsValid(ak string, claims *jwtClaim) error{
	if claims.Ak != ak{
		return errors.New("the actual ak and ak in token is mismatched!")
	} else if time.Now().Unix() > claims.ExpiresAt{
		return errors.New("the token is expired!")
	}else{
		return nil
	}
}