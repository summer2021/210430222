package jwt

import (
	"bytes"
	"fmt"
	"github.com/apache/dubbo-go-pixiu/pkg/context/mock"
	"github.com/apache/dubbo-go-pixiu/pkg/filter/recovery"
	"github.com/dubbogo/dubbo-go-pixiu-filter/pkg/api/config"
	"github.com/dubbogo/dubbo-go-pixiu-filter/pkg/router"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"time"
)

func TestJwt(t *testing.T){
	testAk := "testAccessKey"
	rightTime := time.Now()

	rightSignature := Sign(testAk)
	right, err := Verify(testAk, rightSignature, rightTime)
	assert.Equal(t, true, right)
	assert.NoError(t, err)

	wrongSignature := Sign(testAk + "Wrong")
	wrongTime := rightTime.Add(-25 * time.Hour)
	wrong1, error1 := Verify(testAk, wrongSignature, rightTime)
	assert.Equal(t, false, wrong1)
	assert.Error(t, error1)
	wrong2, error2 := Verify(testAk, rightSignature, wrongTime)
	assert.Equal(t, false, wrong2)
	assert.Error(t, error2)

	token, tokenErr := CreateToken(testAk, rightSignature, rightTime)
	assert.NoError(t, tokenErr)
	claim, parseErr := ParseToken(token)
	assert.Equal(t, testAk, claim.Ak)
	assert.NoError(t, parseErr)
}

func TestAll(t *testing.T){
	testAk := "testAccessKey"
	rightSignature := Sign(testAk)
	now := time.Now()

	p := Plugin{}
	f, _ := p.CreateFilter()
	err := f.Apply()
	assert.Nil(t, err)

	api := router.API{
		URLPattern: "/mock/:ak/:signature/:time",
		Method : getMockMethod(config.MethodGet),
		Headers: make(map[string]string),
	}
	req, err := http.NewRequest("POST","http://www.dubbogopixiu.com/mock", bytes.NewReader(
		[]byte("{\"access_key\":" + testAk + ", \"signature\":" + rightSignature + ", \"start_time\":" + now.String() + "}")))
	assert.NoError(t, err)
	fmt.Println(req)
	req.Header.Set("access_key", testAk)
	req.Header.Set("signature", rightSignature)
	req.Header.Set("start_time", now.String())
	fmt.Println(req.Header)
	c := mock.GetMockHTTPContext(req, f, recovery.GetMock())
	c.API(api)
	c.Next()
	api.Headers["access_key"] = testAk
	api.Headers["signature"] = rightSignature
	api.Headers["start_time"] = now.String()

}

func getMockMethod(verb config.HTTPVerb) config.Method {
	inbound := config.InboundRequest{}
	integration := config.IntegrationRequest{}
	return config.Method{
		OnAir:              true,
		HTTPVerb:           verb,
		InboundRequest:     inbound,
		IntegrationRequest: integration,
	}
}